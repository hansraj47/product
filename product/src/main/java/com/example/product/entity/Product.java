package com.example.product.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

@Entity
@Table(name="product")
public class Product implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Size(min=2,max=20)
	@Column(name="product_name")
	private String productName;
	
	@Column(name="product_category")
	private String productCategory;
	
	@DecimalMin(value="300",inclusive=false)
	@Column(name="min_investment_allowed")
	private int minInvestmentAllowed;
	
	@DecimalMax(value="500000",inclusive=false)
	@Column(name="max_investment_allowed")
	private int maxInvestmentAllowed;
	
	@Column(name="is_active")
	private boolean isActive;
	
	public Product() {
		
	}

	public Product(String productName, String productCategory, int minInvestmentAllowed, int maxInvestmentAllowed,
			boolean isActive) {
	
		this.productName = productName;
		this.productCategory = productCategory;
		this.minInvestmentAllowed = minInvestmentAllowed;
		this.maxInvestmentAllowed = maxInvestmentAllowed;
		this.isActive = isActive;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public int getMinInvestmentAllowed() {
		return minInvestmentAllowed;
	}

	public void setMinInvestmentAllowed(int minInvestmentAllowed) {
		this.minInvestmentAllowed = minInvestmentAllowed;
	}

	public int getMaxInvestmentAllowed() {
		return maxInvestmentAllowed;
	}

	public void setMaxInvestmentAllowed(int maxInvestmentAllowed) {
		this.maxInvestmentAllowed = maxInvestmentAllowed;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + ", productCategory=" + productCategory
				+ ", minInvestmentAllowed=" + minInvestmentAllowed + ", maxInvestmentAllowed=" + maxInvestmentAllowed
				+ ", isActive=" + isActive + "]";
	}
	
	
}
