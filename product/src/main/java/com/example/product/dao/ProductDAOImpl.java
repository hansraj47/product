package com.example.product.dao;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.product.entity.Product;

@Repository
public class ProductDAOImpl implements ProductDAO {
	
	
	private EntityManager entityManager;
	
	@Autowired
	public ProductDAOImpl(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}

	@Override
	public List<Product> findAll() {
		
		Session theSession = entityManager.unwrap(Session.class);
		
		Query<Product> theQuery = theSession.createQuery("from Product",Product.class);
		
		List<Product> theProduct = theQuery.getResultList();
		return theProduct;
	}

	@Override
	public List<Product> findProductById(int theId) {
		
		Session theSession = entityManager.unwrap(Session.class);
		
		Query<Product> theQuery = theSession.createQuery("from Product where id=:productId",Product.class);
		theQuery.setParameter("productId", theId);
		
		List<Product> theProduct = theQuery.getResultList();
		return theProduct;
	}

	@Override
	public void saveProduct(Product theProduct) {
		
		Session theSession = entityManager.unwrap(Session.class);
		
		theSession.saveOrUpdate(theProduct);
		
	}

	@Override
	public void deleteProduct(int theId) {
		
		Session theSession = entityManager.unwrap(Session.class);
		Query theQuery = theSession.createQuery("delete from Product where id=:productId");
		theQuery.setParameter("productId", theId);
		
		theQuery.executeUpdate();
		
	}

}
