package com.example.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.product.dao.ProductDAO;
import com.example.product.entity.Product;

@Service
public class ProductServiceImpl implements ProductService {

	private ProductDAO productDAO;
	
	@Autowired
	public ProductServiceImpl(ProductDAO theProductDAO) {
		productDAO = theProductDAO;	
	}
	@Override
	@Transactional
	public List<Product> findAll() {
		
		return productDAO.findAll();
	}

	@Override
	@Transactional
	public List<Product> findProductById(int theId) {
	
		return productDAO.findProductById(theId);
	}

	@Override
	@Transactional
	public void saveProduct(Product theProduct) {
		
		 productDAO.saveProduct(theProduct);

	}

	@Override
	@Transactional
	public void deleteProduct(int theId) {
		
		productDAO.deleteProduct(theId);
	}

}
