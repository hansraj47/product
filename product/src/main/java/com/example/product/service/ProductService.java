package com.example.product.service;

import java.util.List;

import com.example.product.entity.Product;

public interface ProductService {
	
	public List<Product> findAll();
	
	public List<Product> findProductById(int theId);
	
	public void saveProduct(Product theProduct);
		
	public void deleteProduct(int theId);
	
}
