package com.example.product.rest;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.product.entity.Product;
import com.example.product.service.ProductService;

@RestController
@RequestMapping("/")
@EnableCaching
public class ProductRestController {
	
	private ProductService productService;
	
	@Autowired
	public ProductRestController(ProductService theProductService) {
		productService = theProductService;
	}
	
	Logger logger = LoggerFactory.getLogger(ProductRestController.class);
	
	// method to get list of products
	@GetMapping("/products")
	public List<Product> findAll(){
		
		logger.info("retrieving products data");
		
		return productService.findAll();
	}
	
	// method to get product by Id
	@GetMapping("/products/{productId}")
	@Cacheable(key = "#productId",value = "Product")
	public List<Product> findProductById(@PathVariable int productId){
		
		logger.info("retrieving products data with id " + productId);
		
		return productService.findProductById(productId);
	}
	
	// method to write new product
	@PostMapping("/products")
	public Product saveProduct(@Valid @RequestBody Product theProduct) {
		
		logger.info("writing new Product Data");
		
		theProduct.setId(0);
		productService.saveProduct(theProduct);
		return theProduct;
	}
	
	// method to update existing product
	@PutMapping("/products")
	public Product updateProduct(@RequestBody Product theProduct) {
		
		logger.info("Updating Products Data");
		
		productService.saveProduct(theProduct);
		return theProduct;
	}
	
	// method to delete product
	@DeleteMapping("/products/{productId}")
	@CacheEvict(key = "#productId",value = "Product")
	public String deleteProduct(@PathVariable int productId) {
		
		List<Product> theProduct = productService.findProductById(productId);
		
		if(theProduct == null) {
			throw new RuntimeException("Product not found - " + productId);
		
		
		}
		
		logger.info("Deleting products data with id: " + productId);
		
		productService.deleteProduct(productId);
		return "Deleted Product with product id:" + productId;
	}
	
}
